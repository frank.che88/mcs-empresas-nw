package com.springboot.api.jdbc.dao;

import java.util.List;

import com.springboot.api.jdbc.model.Empresa;

public interface EmpresaDao {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);
	void deleteEmpresa(Integer id);

}
