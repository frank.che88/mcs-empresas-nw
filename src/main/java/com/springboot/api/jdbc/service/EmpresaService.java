package com.springboot.api.jdbc.service;

import java.util.List;

import com.springboot.api.jdbc.model.Empresa;

public interface EmpresaService {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);
	void deleteEmpresa(Integer id);
	
}
