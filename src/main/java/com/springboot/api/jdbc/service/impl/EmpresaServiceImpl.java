package com.springboot.api.jdbc.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.jdbc.dao.impl.EmpresaDaoImpl;
import com.springboot.api.jdbc.model.Empresa;
import com.springboot.api.jdbc.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmpresaDaoImpl _empresaDao;
	
	@Override
	public List<Empresa> getAllEmpresas() {
		
		return _empresaDao.getAllEmpresas();
	}

	@Override
	public Empresa getEmpresa(Integer id) {
		
		return _empresaDao.getEmpresa(id);
	}

	@Override
	public void saveEmpresa(Empresa empresa) {
		try {
			_empresaDao.saveEmpresa(empresa);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}

	@Override
	public void deleteEmpresa(Integer id) {
		try {
			_empresaDao.deleteEmpresa(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}

}
