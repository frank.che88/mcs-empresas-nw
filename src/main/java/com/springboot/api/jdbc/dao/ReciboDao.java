package com.springboot.api.jdbc.dao;

import java.util.List;

import com.springboot.api.jdbc.model.Recibo;

public interface ReciboDao {
	
	
	List<Recibo> getReciboByEmpresa(Integer idEmpresa);
	void saveRecibo(Recibo recibo);
	

}
