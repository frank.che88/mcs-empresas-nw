package com.springboot.api.jdbc.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.jdbc.dao.impl.ReciboDaoImpl;
import com.springboot.api.jdbc.model.Recibo;
import com.springboot.api.jdbc.service.ReciboService;

@Service
public class ReciboServiceImpl implements ReciboService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ReciboDaoImpl _reciboDao;
	
	

	@Override
	public void saveRecibo(Recibo recibo) {
		try {
			_reciboDao.saveRecibo(recibo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}

	

}
