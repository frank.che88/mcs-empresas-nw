package com.springboot.api.jdbc.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.jdbc.dao.EmpresaDao;
import com.springboot.api.jdbc.model.Empresa;
import com.springboot.api.jdbc.rowmapper.EmpresaRowMapper;

@Repository
public class EmpresaDaoImpl extends JdbcDaoSupport implements EmpresaDao {

	public EmpresaDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Empresa> getAllEmpresas() {
		
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id, ruc, razon_social, estado_actual FROM empresa";
		
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			logger.debug("Se han listado "+listaEmpresas.size()+" empresas");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaEmpresas;
	}

	@Override
	public Empresa getEmpresa(Integer id) {
		Empresa empresa = new Empresa();	
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id, ruc, razon_social, estado_actual\n" + 
				" FROM empresa where id='"+id+"'";
				
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			
			Object[] params = {id};
			
			listaEmpresas = getJdbcTemplate().query     (sql, empresaRow);
			
			empresa = listaEmpresas.get(0);
			
			logger.debug("Se ha traido a la persona "+listaEmpresas.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return empresa;
	}

	@Override
	public void saveEmpresa(Empresa empresa) {
		
		String sql = "insert into empresa (id, ruc, razon_social, estado_actual) "  
				+ "values (?, ?, ?, ?);";
		
		Object[] params = { empresa.getId(), empresa.getRuc(), empresa.getRazon_social(), empresa.getEstado_actual()};
		int[] tipos = { Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado el empresa "+empresa.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	@Override
	public void deleteEmpresa(Integer id) {
		int regeliminados = 0;		
		String sql = " delete from empresa where id ='"+id+"'";		
		try {			
			regeliminados = getJdbcTemplate().update(sql);
			logger.debug("Se han eliminado "+regeliminados+" empresas con id = "+id);
		} catch (Exception e) {			
			logger.error(e.getMessage());
		}
	}

}
