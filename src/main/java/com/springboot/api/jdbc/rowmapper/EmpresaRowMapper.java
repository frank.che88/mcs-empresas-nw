package com.springboot.api.jdbc.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.jdbc.model.Empresa;

public class EmpresaRowMapper implements RowMapper<Empresa>{

	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		Empresa empresa = new Empresa();
		
		empresa.setId(rs.getInt("id"));
		empresa.setRuc(rs.getString("ruc"));
		empresa.setRazon_social(rs.getString("razon_social"));
		empresa.setEstado_actual(rs.getString("estado_actual"));
		
		/*
		if(rs.getInt("estado_actual")) {
			
		}
		*/
		
		
		return empresa;
	}

}
