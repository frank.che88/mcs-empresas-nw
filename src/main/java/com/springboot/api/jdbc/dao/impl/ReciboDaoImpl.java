package com.springboot.api.jdbc.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.jdbc.dao.ReciboDao;
import com.springboot.api.jdbc.model.Recibo;
import com.springboot.api.jdbc.rowmapper.ReciboRowMapper;

@Repository
public class ReciboDaoImpl extends JdbcDaoSupport implements ReciboDao {

	public ReciboDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	

	@Override
	public List<Recibo> getReciboByEmpresa(Integer id) {
			
		List<Recibo> listaEmpresas = new ArrayList<Recibo>();
		
		String sql = " SELECT id, ruc, razon_social, estado_actual\n" + 
				" FROM recibo where id_empresa = '" + id + "'";
				
		try {
			
			RowMapper<Recibo> empresaRow = new ReciboRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			
		
			
			logger.debug("Se ha traido a la persona "+listaEmpresas.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaEmpresas;
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		
		String sql = "insert into empresa (id_empresa, nro_recibo, monto_emitido, fg_retencion) "  
				+ "values (?, ?, ?, ?);";
		
		Object[] params = { recibo.getId_empresa(), recibo.getNro_recibo(), recibo.getMonto_emitido(), recibo.getFg_retencion()};
		int[] tipos = { Types.INTEGER, Types.INTEGER, Types.DOUBLE,  Types.INTEGER};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado el empresa "+recibo.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

}
