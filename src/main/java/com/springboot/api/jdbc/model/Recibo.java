package com.springboot.api.jdbc.model;

public class Recibo {
	
	private Integer nro_recibo;
	private Integer id_empresa;
	private Double monto_emitido;
	private Integer fg_retencion;
	
	
	
	public Recibo() {
		super();
		
	}




	public Recibo(Integer nro_recibo, Integer id_empresa, Double monto_emitido, Integer fg_retencion) {
		super();
		this.nro_recibo = nro_recibo;
		this.id_empresa = id_empresa;
		this.monto_emitido = monto_emitido;
		this.fg_retencion = fg_retencion;
	}




	public Integer getId_empresa() {
		return id_empresa;
	}




	public void setId_empresa(Integer id_empresa) {
		this.id_empresa = id_empresa;
	}




	public Integer getNro_recibo() {
		return nro_recibo;
	}



	public void setNro_recibo(Integer nro_recibo) {
		this.nro_recibo = nro_recibo;
	}



	public Double getMonto_emitido() {
		return monto_emitido;
	}



	public void setMonto_emitido(Double monto_emitido) {
		this.monto_emitido = monto_emitido;
	}



	public Integer getFg_retencion() {
		return fg_retencion;
	}



	public void setFg_retencion(Integer fg_retencion) {
		this.fg_retencion = fg_retencion;
	}
	
	
	

}
