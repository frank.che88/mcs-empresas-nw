package com.springboot.api.jdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.jdbc.model.Empresa;
import com.springboot.api.jdbc.service.impl.EmpresaServiceImpl;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@GetMapping(value = "/", produces = "application/json")	
	public EmpresaResponse getAllEmpresas(){
		return new EmpresaResponse(_empresaService.getAllEmpresas());
	}
	
	@GetMapping(value = "/{id_empresa}", produces = "application/json")	
	public EmpresaResponse getEmpresa(@PathVariable ("id_empresa") Integer id){
		return new EmpresaResponse (_empresaService.getEmpresa(id));
	}
	
	@PostMapping(value = "/", produces = "application/json")	
	public EmpresaResponse saveEmpresa(@RequestBody Empresa empresa){
		
		_empresaService.saveEmpresa(empresa);
		return new EmpresaResponse("Empresa registrada con éxito");
	}
	
	public static class EmpresaResponse {
		private String codigo_servio = "0000";
		private String descripcion;
		private List<Empresa> empresas;
		private Empresa empresa;
		
		
		
		
		public EmpresaResponse(Empresa empresa) {
			super();
			this.empresa = empresa;
		}



		public EmpresaResponse(List<Empresa> empresas) {
			super();
			this.empresas = empresas;
		}



		public EmpresaResponse(String descripcion) {
			super();
			this.descripcion = descripcion;
		}
		
		
		
		public String getCodigo_servio() {
			return codigo_servio;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public List<Empresa> getEmpresas() {
			return empresas;
		}
		public Empresa getEmpresa() {
			return empresa;
		}
		
		
		
	}
	

}
