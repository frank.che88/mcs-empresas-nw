package com.springboot.api.jdbc.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.jdbc.model.Recibo;

public class ReciboRowMapper implements RowMapper<Recibo>{

	@Override
	public Recibo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Recibo recibo = new Recibo();
		
		recibo.setNro_recibo(rs.getInt("Recibo"));
		recibo.setId_empresa(rs.getInt("id_empresa"));
		recibo.setMonto_emitido(rs.getDouble("monto_emitido"));
		recibo.setFg_retencion(rs.getInt("fg_retencion"));
		
		
		
		return recibo;
	}

}
