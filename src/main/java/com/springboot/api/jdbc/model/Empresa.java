package com.springboot.api.jdbc.model;

public class Empresa {

	private Integer id;
	private String ruc;
	private String razon_social;
	private String estado_actual;
	

	public Empresa() {
		
	}


	public Empresa(Integer id, String ruc, String razon_social, String estado_actual) {
		super();
		this.id = id;
		this.ruc = ruc;
		this.razon_social = razon_social;
		this.estado_actual = estado_actual;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getRuc() {
		return ruc;
	}


	public void setRuc(String ruc) {
		this.ruc = ruc;
	}


	public String getRazon_social() {
		return razon_social;
	}


	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}


	public String getEstado_actual() {
		return estado_actual;
	}


	public void setEstado_actual(String estado_actual) {
		this.estado_actual = estado_actual;
	}

	

}
