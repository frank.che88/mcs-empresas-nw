package com.springboot.api.jdbc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.jdbc.model.Recibo;
import com.springboot.api.jdbc.service.impl.ReciboServiceImpl;

@RestController
@RequestMapping("/recibo")
public class ReciboController {
	
	@Autowired
	private ReciboServiceImpl _productoService;
	
	@PostMapping(value = "/", produces = "application/json")	
	public ReciboResponse saveRecibo(@RequestBody Recibo recibo){
		
		_productoService.saveRecibo(recibo);
		return new ReciboResponse();
	}	
	
	
	
	public static class ReciboResponse {
		private String codigo_servicio = "0000";
		private String descripcion = "Recibo registrado con exito";
		public String getCodigo_servicio() {
			return codigo_servicio;
		}
		public void setCodigo_servicio(String codigo_servicio) {
			this.codigo_servicio = codigo_servicio;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
	}

}
